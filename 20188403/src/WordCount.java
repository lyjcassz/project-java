import java.io.*;

public class WordCount {
    public static void main(String[] args)  {

        String input = args[0];
        String output = args[1];

        if(args.length != 2){
            
            System.out.println("用java WordCount输入.txt输出.txt！");
            System.exit(2);
        }t
        
        //WordCountCore中的countFrequency函数以一列垂直数组返回一组数据的频率分布
        try {
            WordCountCore.countWordFrequency(input, output);
        }catch (IOException e){
            System.out.println("[-]IO Exception: " + e);
        }
    }

}