import java.*;

public class Lib{

    private static final Pattern pattern = Pattern.compile("[a-zA-Z]{4}[a-zA-Z0-9]*");

    BufferedReader br = new BufferedReader(new FileReader(inputFile));
    String line = null;
    //一行一行读取文件
    while((line = br.readLine()) != null){
        //分割符：空格，非字母数字符号,以分割符来分割出单词
        String[] words = line.split("[^a-zA-Z0-9]");
        //单词：至少以4个英文字母开头
        for (String word : words){
            if (word.length() >= 4) {
                //正则表达式判断单词是否合法
                if (pattern.matcher(word).matches()){
                    //统一转小写
                    wordList.add(word.toLowerCase());
                }
            }
        }
    }     

        /*统计词频，只输出出现最多的10个*/
    public ArrayList countWordFrequency(String inputFile)
    {
        List<String> wordList = getWordList(inputFile);
        //key 单词  value 出现次数
        Map<String, Integer> words = new TreeMap<String,Integer>();

        //如果有这个单词 count ++
        for (String word : wordList){
            if (words.containsKey(word))
                words.put(word,words.get(word)+1);
            else {
                //如果map里没有这个单词，添加进去，count=1
                words.put(word, 1);
            }
        }
        //按值进行排序,sortMap是自定义的用来按value排序的函数
        ArrayList<Map.Entry<String,Integer>> list = sortMap(words);

        return list;
    }

    List<String> wordList = getWordList(inputFile);
    //单词列表的长度
    int count = wordList.size();

    BufferedReader br = new BufferedReader(new FileReader(inputFile));
    //每次读一个字符
    while ((br.read()) != -1)
    count++;

    BufferedReader br = new BufferedReader(new FileReader(inputFile));
    String line = "";
    while((line = br.readLine()) != null){
        //判断是否为空白行
    if (!(line.trim().isEmpty()))
        count++;
    }

        //单元测试

    /*测试有几个单词*/
    @Test
    public void testCountWords(){
        Lib lib = new Lib("input.txt","output.txt");
        Assert.assertEquals(5,lib.countWords("input1.txt"));
    }

    /*测试有几个字符*/
    @Test
    public void testCountChars() {
        Lib lib = new Lib("input.txt","output.txt");
        Assert.assertEquals(88,lib.countChars("input1.txt"));
    }

    /*测试词频统计*/
    @Test
    public void testCountWordFrequency(){
        Lib lib = new Lib("input.txt","output.txt");
        System.out.println("input1.txt");
        lib.printWords(lib.countWordFrequency("input1.txt"));
    }

    /*测试有几行*/
    @Test
    public void testCountLines(){
        Lib lib = new Lib("input.txt","output.txt");
        Assert.assertEquals(4,lib.countLines("input1.txt"));
    }

    @Test
    public void TestMassiveData()
    {
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter("massinput.txt"));

            //(共30个有效字符、3个单词) * n
            for (int i=0; i<1000000;i++)
            bw.write("aabbccDDEfg,hijlmn123 \nINPUT\r\n");
            bw.close();

            Lib lib = new Lib("massinput.txt","massoutput.txt");
            //计算
            lib.startCount();
            //输出
            lib.outputResult();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
}


